package gameoflife;

import javax.swing.JLabel;
import javax.swing.Timer;

/**
 * Used to select the type of pre-defined seed pattern for the simulation.
 */
enum GamePattern {
    GLIDER,
    GLIDER_GUN,
    BLINKER,
    PENTADECATHLON
}

/**
 * This class holds a simple implementation of Conway's Game of Life. The simulation
 * consists of dead and live cells that react based on 4 simple rules. This class
 * allows for the simulation to be started and stopped at will. It also contains
 * several pre-defined seed patterns that can be switched at any time.
 */
class GameOfLife {

    private Grid grid;
    private JLabel outputLabel;
    private Timer timer;

    // Simulation animation speed
    private final int SPEED = 70;

    /**
     * GameOfLife constructor. Creates a blank grid and assigns a starting pattern.
     * @param rows Number of rows to make the grid.
     * @param columns Number of columns to make the grid.
     * @param outputLabel Reference to a JLabel to print the grid to.
     */
    GameOfLife(int rows, int columns, JLabel outputLabel) {
        grid = new Grid(rows, columns, '.', '#');
        this.outputLabel = outputLabel;

        // Timer used to start and stop the simulation
        timer = new Timer(SPEED, e -> runSimulation());

        // Sets starting pattern to a Glider
        SetPattern(GamePattern.GLIDER);
    }

    /**
     * Starts the simulation based on the currently selected pattern.
     */
    void Start() {
        timer.start();
    }

    /**
     * Moves the simulation one tick forward.
     */
    void Next() {
        timer.stop();
        runSimulation();
    }

    /**
     * Stops the simulation.
     */
    void Stop() {
        timer.stop();
    }

    /**
     * Runs the Game of Life simulation. Each tick, the algorithm decides which cells
     * should live and which cells should die. This grid is then printed out.
     */
    private void runSimulation() {
        // Iterate through entire grid
        for (int i = 0; i < grid.NumberOfRows(); ++i) {
            for (int j = 0; j < grid.NumberOfColumns(); ++j) {
                // The number of live neighbors at this current location
                int neighbors = grid.AliveNeighborsAt(i, j);

                if (grid.IsAliveAt(i, j)) {
                    if (neighbors < 2 || neighbors > 3) {
                        // Die by underpopulation or overpopulation
                        grid.MarkDead(i, j);
                    }
                } else if (grid.IsDeadAt(i, j)) {
                    if (neighbors == 3) {
                        // Live by reproduction
                        grid.MarkAlive(i, j);
                    }
                }
            }
        }

        // "Locks in" the cells marked as alive or dead
        grid.UpdateGrid();

        outputGrid();
    }

    /**
     * Sets the initial pattern of the simulation.
     * @param pattern Pattern to start the simulation.
     */
    void SetPattern(GamePattern pattern) {
        switch (pattern) {
            case GLIDER:
                grid.SeedGrid(new int[][]{
                        {2, 0},
                        {0, 1},
                        {2, 1},
                        {1, 2},
                        {2, 2}
                });
                break;
            case GLIDER_GUN:
                grid.SeedGrid(new int[][]{
                        {7, 12},
                        {8, 12},
                        {7, 13},
                        {8, 13},
                        {7, 22},
                        {8, 22},
                        {9, 22},
                        {6, 23},
                        {10, 23},
                        {5, 24},
                        {11, 24},
                        {5, 25},
                        {11, 25},
                        {8, 26},
                        {6, 27},
                        {10, 27},
                        {7, 28},
                        {8, 28},
                        {9, 28},
                        {8, 29},
                        {5, 32},
                        {6, 32},
                        {7, 32},
                        {5, 33},
                        {6, 33},
                        {7, 33},
                        {4, 34},
                        {8, 34},
                        {3, 36},
                        {4, 36},
                        {8, 36},
                        {9, 36},
                        {5, 46},
                        {6, 46},
                        {5, 47},
                        {6, 47}
                });
                break;
            case BLINKER:
                grid.SeedGrid(new int[][]{
                        {9, 18},
                        {10, 18},
                        {11, 18},
                        {0, 27},
                        {1, 27},
                        {2, 27},
                        {10, 40},
                        {11, 40},
                        {12, 40},
                        {17, 43},
                        {18, 43},
                        {19, 43}
                });
                break;
            case PENTADECATHLON:
                grid.SeedGrid(new int[][]{
                        {6, 28},
                        {7, 28},
                        {8, 28},
                        {9, 28},
                        {10, 28},
                        {11, 28},
                        {12, 28},
                        {13, 28},
                        {6, 29},
                        {8, 29},
                        {9, 29},
                        {10, 29},
                        {11, 29},
                        {13, 29},
                        {6, 30},
                        {7, 30},
                        {8, 30},
                        {9, 30},
                        {10, 30},
                        {11, 30},
                        {12, 30},
                        {13, 30}
                });
                break;
            default:
                break;
        }

        // Display the grid
        outputGrid();
    }

    /**
     * Displays the grid in a given JLabel object.
     */
    private void outputGrid() {
        String formattedText = grid.toString().replace("\n", "<br>");
        outputLabel.setText("<html><font face=\"monospace\">" + formattedText + "</html>");
    }
}
