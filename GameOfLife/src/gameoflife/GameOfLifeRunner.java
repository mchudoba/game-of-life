package gameoflife;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;

/**
 * Main class that runs the Game of Life simulation. This class creates a GUI using
 * a JFrame to hold the simulation grid as well as buttons to interact with it.
 */
public class GameOfLifeRunner {

    /**
     * Main method.
     * @param args optional arguments. None are expected.
     */
    public static void main(String[] args) {
        // Create the frame and label for Game of Life grid
        JFrame outputFrame = new JFrame("Conway's Game of Life");
        JLabel gridLabel = new JLabel("", SwingConstants.CENTER);

        GameOfLife game = new GameOfLife(20, 60, gridLabel);

        // Set frame properties
        outputFrame.setResizable(false);
        outputFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        outputFrame.setLayout(new BoxLayout(outputFrame.getContentPane(), BoxLayout.Y_AXIS));
        outputFrame.setPreferredSize(new Dimension(800, 600));

        Dimension buttonSize = new Dimension(110, 40);

        // Create pattern buttons
        JButton gliderButton = new JButton("Glider");
        gliderButton.setPreferredSize(buttonSize);
        gliderButton.addActionListener(e -> game.SetPattern(GamePattern.GLIDER));

        JButton gliderGunButton = new JButton("Glider Gun");
        gliderGunButton.setPreferredSize(buttonSize);
        gliderGunButton.addActionListener(e -> game.SetPattern(GamePattern.GLIDER_GUN));

        JButton blinkerButton = new JButton("Blinker");
        blinkerButton.setPreferredSize(buttonSize);
        blinkerButton.addActionListener(e -> game.SetPattern(GamePattern.BLINKER));

        JButton pentadecathlonButton = new JButton("Pentadecathlon");
        pentadecathlonButton.setPreferredSize(buttonSize);
        pentadecathlonButton.addActionListener(e -> game.SetPattern(GamePattern.PENTADECATHLON));

        // Create control buttons
        JButton startButton = new JButton("Start");
        startButton.setPreferredSize(buttonSize);
        startButton.addActionListener(e -> game.Start());

        JButton nextButton = new JButton("Next");
        nextButton.setPreferredSize(buttonSize);
        nextButton.addActionListener(e -> game.Next());

        JButton stopButton = new JButton("Stop");
        stopButton.setPreferredSize(buttonSize);
        stopButton.addActionListener(e -> game.Stop());

        // Create quit button
        JButton quitButton = new JButton("Quit");
        quitButton.setPreferredSize(buttonSize);
        quitButton.addActionListener(e -> System.exit(0));

        // Create and fill panels
        JPanel patternPanel = new JPanel();
        patternPanel.setBorder(new EmptyBorder(20, 10, 0, 10));
        patternPanel.add(new JLabel("Select a Pattern"));
        patternPanel.add(gliderButton);
        patternPanel.add(gliderGunButton);
        patternPanel.add(blinkerButton);
        patternPanel.add(pentadecathlonButton);

        JPanel gridPanel = new JPanel();
        gridPanel.add(gridLabel);

        JPanel controlPanel = new JPanel();
        controlPanel.add(startButton);
        controlPanel.add(nextButton);
        controlPanel.add(stopButton);

        JPanel quitPanel = new JPanel();
        quitPanel.add(quitButton);

        // Add panels to frame
        outputFrame.getContentPane().add(patternPanel);
        outputFrame.getContentPane().add(gridPanel);
        outputFrame.getContentPane().add(controlPanel);
        outputFrame.getContentPane().add(quitPanel);

        // Display frame
        outputFrame.pack();
        outputFrame.setVisible(true);
    }
}
