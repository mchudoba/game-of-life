package gameoflife;

/**
 * This class abstracts behavior for a Game of Life grid. The grid can be seeded
 * with a pattern to run the simulation with. The grid consists of "cells" that
 * can be either alive or dead.
 */
class Grid {

    private char[][] grid;
    private int rows, columns;
    private char deadChar, aliveChar;

    // Characters used to mark cells as alive or dead before the next tick
    private final char deadMarkChar = 'd';
    private final char aliveMarkChar = 'a';

    /**
     * Grid constructor. Creates a new grid with the given size constraints
     * and character values.
     * @param rows Number of rows to create the grid.
     * @param columns Number of columns to create the grid.
     * @param deadChar Character to represent dead cells.
     * @param aliveChar Character to represent alive cells.
     */
    Grid(int rows, int columns, char deadChar, char aliveChar) {
        this.rows = rows;
        this.columns = columns;
        this.deadChar = deadChar;
        this.aliveChar = aliveChar;

        grid = new char[this.rows][this.columns];

        // Grid starts out all dead
        clearGrid();
    }

    /**
     * Seeds the grid with alive cells. The locations given are all marked as
     * alive in the grid.
     * @param locations 2D array of locations to mark as alive. This array MUST
     *                  contain pairs of integers, for example, {{0, 0}, {1, 1}, ...}
     */
    void SeedGrid(int[][] locations) {
        clearGrid();

        for (int[] loc : locations) {
            setAlive(loc[0], loc[1]);
        }
    }

    /**
     * Returns the grid as a string.
     * @return Grid as a string with newlines after each row.
     */
    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for (char[] row : grid) {
            output.append(row);
            output.append('\n');
        }

        return output.toString();
    }

    /**
     * Returns the number of rows in the grid.
     * @return Number of rows.
     */
    int NumberOfRows() {
        return rows;
    }

    /**
     * Returns the number of columns in the grid.
     * @return Number of columns.
     */
    int NumberOfColumns() {
        return columns;
    }

    /**
     * Returns true if the cell at the given index is dead.
     * @param rowIndex Row index in the grid.
     * @param colIndex Column index in the grid.
     * @return True if the cell is dead, false otherwise.
     */
    boolean IsDeadAt(int rowIndex, int colIndex) {
        char current = grid[rowIndex][colIndex];
        return current == deadChar || current == aliveMarkChar;
    }

    /**
     * Returns true if the cell at the given index is alive.
     * @param rowIndex Row index in the grid.
     * @param colIndex Column index in the grid.
     * @return True if the cell is alive, false otherwise.
     */
    boolean IsAliveAt(int rowIndex, int colIndex) {
        char current = grid[rowIndex][colIndex];
        return current == aliveChar || current == deadMarkChar;
    }

    /**
     * Marks a live cell as dead. The cell is still treated as alive for the
     * remainder of the current tick.
     * @param rowIndex Row index in the grid.
     * @param colIndex Column index in the grid.
     */
    void MarkDead(int rowIndex, int colIndex) {
        grid[rowIndex][colIndex] = deadMarkChar;
    }

    /**
     * Marks a dead cell as alive. The cell is still treated as dead for the
     * remainder of the current tick.
     * @param rowIndex Row index in the grid.
     * @param colIndex Column index in the grid.
     */
    void MarkAlive(int rowIndex, int colIndex) {
        grid[rowIndex][colIndex] = aliveMarkChar;
    }

    /**
     * Updates all cells in the grid that are marked. Cells marked as alive
     * are made alive, cells marked as dead are made dead.
     */
    void UpdateGrid() {
        for (char[] row : grid) {
            for (int i = 0; i < columns; ++i) {
                if (row[i] == deadMarkChar) {
                    row[i] = deadChar;
                } else if (row[i] == aliveMarkChar) {
                    row[i] = aliveChar;
                }
            }
        }
    }

    /**
     * Calculates the number of alive neighbors at a given location. Cells can
     * have a maximum of 8 neighbors - above, below, left, right, and diagonal.
     * Walls are treated as dead cells.
     * @param rowIndex Row index in the grid.
     * @param colIndex Column index in the grid.
     * @return The number of alive neighbors of the given cell.
     */
    int AliveNeighborsAt(int rowIndex, int colIndex) {
        int alive = 0;

        boolean leftExists = colIndex - 1 >= 0;
        boolean rightExists = colIndex + 1 < columns;
        boolean topExists = rowIndex - 1 >= 0;
        boolean bottomExists = rowIndex + 1 < rows;

        // Left
        if (leftExists && IsAliveAt(rowIndex, colIndex - 1)) {
            ++alive;
        }

        // Top left
        if (topExists && leftExists && IsAliveAt(rowIndex - 1, colIndex - 1)) {
            ++alive;
        }

        // Top
        if (topExists && IsAliveAt(rowIndex - 1, colIndex)) {
            ++alive;
        }

        // Top right
        if (topExists && rightExists && IsAliveAt(rowIndex - 1, colIndex + 1)) {
            ++alive;
        }

        // Right
        if (rightExists && IsAliveAt(rowIndex, colIndex + 1)) {
            ++alive;
        }

        // Bottom right
        if (bottomExists && rightExists && IsAliveAt(rowIndex + 1, colIndex + 1)) {
            ++alive;
        }

        // Bottom
        if (bottomExists && IsAliveAt(rowIndex + 1, colIndex)) {
            ++alive;
        }

        // Bottom left
        if (bottomExists && leftExists && IsAliveAt(rowIndex + 1, colIndex - 1)) {
            ++alive;
        }

        return alive;
    }

    /**
     * Sets a cell at a given location as alive immediately.
     * @param rowIndex Row index in the grid.
     * @param colIndex Column index in the grid.
     */
    private void setAlive(int rowIndex, int colIndex) {
        grid[rowIndex][colIndex] = aliveChar;
    }

    /**
     * Clears the grid by marking every cell as dead.
     */
    private void clearGrid() {
        for (char[] row : grid) {
            for (int i = 0; i < columns; ++i) {
                row[i] = deadChar;
            }
        }
    }
}
