# README #

This repository holds a simple implementation of Conway's Game of Life in Java.

There is a build of the application in the GameOfLife/out/artifacts/GameOfLife_jar directory named GameOfLife.jar.

The source code is in the GameOfLife/src/gameoflife directory.

* GameOfLifeRunner.java is the main class of the application. It builds the JFrame GUI and interacts with the game using JButtons.

* GameOfLife.java is the game implementation. It has functionality to start and stop the simulation as well as choose from several pre-defined patterns.

* Grid.java holds a Grid class that makes it simple to interact with the Game of Life grid of alive and dead cells.